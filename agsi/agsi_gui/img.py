

import os
from agsi_gui.qt import QtGui
import qtawesome as qta

# see https://github.com/spyder-ide/qtawesome

from agsi_gui import  ASSETS_DIR


class Ico():


    favicon = "icon_sm.png"
    agsi = "agsi.svg"
    #favicon = ["mdi.home", "#009900"]

    bug = ["mdi.bug", "#009900"]
    bugs = ["mdi.virus", "#009900"]


    quit = "mdi.exit-to-app"

    @staticmethod
    def logo_icon():
        return QtGui.QIcon(os.path.join(ASSETS_DIR, "agsi_icon.svg"))

    @staticmethod
    def i(obj, color=None):
        return Ico.icon(obj, color=color)

    @staticmethod
    def icon(obj, color="#555555"):
        xcolor = color

        if isinstance(obj, QtGui.QIcon):
            return obj

        if isinstance(obj, list):
            xname = obj[0]
            xcolor = obj[1]
        else:
            xname = obj
        if xname.endswith(".png") or xname.endswith(".svg"):
            pth = os.path.join("TODO", xname)
            return QtGui.QIcon(pth)

        return qta.icon(xname, color=xcolor)
