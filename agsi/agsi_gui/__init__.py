
import os
import sys
from agsi_gui.qt import QtWidgets

APP_TITLE = "AGSi Desktop"
APP_VERSION = "0.1-alpha"#


ASSETS_DIR = os.path.join(os.path.dirname(__file__), "..", "static_assets")



def show_desktop():

    from agsi_gui import agsi_main_window

    app = QtWidgets.QApplication(sys.argv)

    wind = agsi_main_window.MainWindow()
    wind.show()

    sys.exit( app.exec_() )


