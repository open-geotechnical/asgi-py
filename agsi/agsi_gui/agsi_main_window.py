
import sys

from agsi_gui import APP_TITLE, APP_VERSION
from agsi_gui.qt import QtCore, QtWidgets, Qt
from agsi_gui.img import Ico

from agsi_gui import example_widgets, xwidgets


class MainWindow(QtWidgets.QMainWindow):

    def on_after(self):
        """This is called after __init__, a few moment later"""

        # self.on_browse_agsi()
        pass


    def __init__( self, ):
        super().__init__()




        ##===============================================
        # Main window stuff
        self.setObjectName("AGSi-main-window")
        QtWidgets.QApplication.setStyle( QtWidgets.QStyleFactory.create( 'Cleanlooks' ) )
        self.setWindowTitle("{} - {}".format(APP_TITLE, APP_VERSION))
        self.setWindowIcon(Ico.logo_icon())


        ##=================================================
        ## Menus - or a welsh meniw ;-)

        #=======
        ## File
        self.menuFile = self.menuBar().addMenu("File")

        self.actionOpen = self.menuFile.addAction("Open Ags4i File", self.on_open_agsi_file)
        self.actionRecent = self.menuFile.addAction("Recent")
        self.actionRecent.setMenu(QtWidgets.QMenu())
        self.actionRecent.menu().triggered.connect(self.on_open_recent)
        self.menuFile.addSeparator()

        self.actionQuit = self.menuFile.addAction(Ico.icon(Ico.quit), "Quit", self.on_quit)


        #=======
        ## View
        self.menuViews = self.menuBar().addMenu("View")
        self.actionAgsiBrowse = self.menuViews.addAction(Ico.logo_icon(), "AGSi Spec", self.on_browse_agsi)
        self.actionAgsiBrowse.setCheckable(True)

        #=======
        ## Examples - its an example widget within
        self.menuExamples = self.menuBar().addMenu("Examples")

        self.widgetActionExamples = QtWidgets.QWidgetAction(self.menuExamples)
        self.examplesWidget = example_widgets.ExamplesWidget(self)
        self.examplesWidget.setMinimumHeight(600)
        self.widgetActionExamples.setDefaultWidget(self.examplesWidget)
        self.examplesWidget.sigFileSelected.connect(self.load_agsi_example)

        self.actionExamples = self.menuExamples.addAction(self.widgetActionExamples)

        self.menuHelp = self.menuBar().addMenu("Help")

        self.menuHelp.addAction("agsi dev docs")

        ##===========================
        ## Top Bar
        self.toolBar = QtWidgets.QToolBar()
        self.toolBar.setContentsMargins(0, 0, 0, 0)
        self.toolBar.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.toolBar.setMovable(False)
        self.addToolBar(Qt.TopToolBarArea, self.toolBar)

        self.toolBar.addAction(self.actionAgsiBrowse)

        self.toolBar.addSeparator()

        ### add a Banner and logo
        logoWidget = QtWidgets.QWidget()
        lwLay = xwidgets.hlayout(margin=0)
        logoWidget.setLayout(lwLay)
        self.toolBar.addWidget(logoWidget)

        self.lblBanner = QtWidgets.QLabel()
        self.lblBanner.setText(APP_TITLE)
        self.lblBanner.setAlignment(Qt.AlignRight|Qt.AlignVCenter)
        self.lblBanner.setSizePolicy(QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum)

        final_color = "#11BB66"
        sty = "font-style:italic; font-weight: bold;  color: #333333; margin: 0; font-size: 12pt; font-family: arial;"
        sty += "padding: 5px;"
        if True:
            sty += "background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, "
            sty += "stop: 0 transparent "
            sty += ", stop: 0.1 #efefef "
            sty += "stop: 1 %s" % final_color
            sty += ");"
        self.lblBanner.setStyleSheet(sty)
        lwLay.addWidget(self.lblBanner)

        iconLabel = QtWidgets.QLabel()
        iconLabel.setStyleSheet("padding: 4px; background-color: %s" % final_color)
        icon = Ico.icon(Ico.favicon)
        iconLabel.setPixmap(icon.pixmap(QtCore.QSize( 25, 25 )))
        lwLay.addWidget(iconLabel)


        ##===============================================
        ## Central widget contains  tabBar and a stack
        centralWidget = QtWidgets.QWidget()
        centralLayout = QtWidgets.QVBoxLayout()
        centralLayout.setContentsMargins(0, 0, 0, 0)
        centralLayout.setSpacing(0)
        centralWidget.setLayout(centralLayout)
        self.setCentralWidget(centralWidget)

        tabContainer = xwidgets.hlayout()
        centralLayout.addLayout(tabContainer)

        self.tabBar = QtWidgets.QTabBar()
        self.tabBar.setMovable(False)
        self.tabBar.setTabsClosable(True)
        tabContainer.addWidget(self.tabBar)
        self.tabBar.currentChanged.connect(self.on_tab_changed)
        self.tabBar.tabCloseRequested.connect(self.on_tab_close_requested)
        tabContainer.addStretch(10)


        self.stackWidget = QtWidgets.QStackedWidget()
        centralLayout.addWidget(self.stackWidget)



        #=========================================
        # Seutp basic window dims, and restore
        self.setMinimumWidth(800)
        self.setMinimumHeight(800)
        #Settings.restore_window( self )
        self.load_recent()

        ## run some stuff a few moments after window shown
        QtCore.QTimer.singleShot(200, self.on_after)





    def closeEvent(self, event):
        pass #QSettings.save_window(self)

    ##=========================================================================================================
    def on_quit(self):
        ret = QtWidgets.QMessageBox.warning(self, "AGSi", "Sure you want to Quit ?",
                                            QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Yes)
        if ret == QtWidgets.QMessageBox.Yes:
            #QSettings.save_window(self)
            sys.exit(0)

    def load_recent(self):
        pass

    def on_open_recent(self):
        pass


    def on_browse_agsi(self):
        pass


    def load_agsi_example(self):
        pass


    def on_open_agsi_file(self):
        pass


    def on_tab_changed(self):
        pass


    def on_tab_close_requested(self):
        pass